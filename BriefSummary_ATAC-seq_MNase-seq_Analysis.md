Brief summary of read processing pipelines for ATAC-seq and MNase-seq data
==========================================================================

Note that there are 6 replicate libraries for the ATAC-seq experiment, and data from two sequencing runs - an initial test
of the library on a Miseq, followed by a full flowcell on a Nextseq500. A single dataset is available for the MNase-seq library,
so that is described first, followed by a description of the six ATAC-seq libraries, followed finally by the joint analysis.

MNase-seq
---------

1. Download files from Novogene FTP server, run FastQC. First 6 bases of each read are lower-quality and have distorted G+C content
adapter contamination around 0.5% also detected.

2. Filter and trim reads using bbduk2.sh - use adapter sequences in ~/software/bbmap/resources/adapters.fa for right-end trimming, 
left-end trim to Q30 in 6-nt window, drop both pairs if either is shorter than 30 nt after trimming.

```console
~/software/bbmap/bbduk2.sh -Xmx20g in=read1.fq.gz in2=read2.fq.gz rref=~/software/bbmap/resources/adapters.fa out=r1.filt.fq out2=r2.filt.fq \   
 stats=filt.stats k=19 minlen=30 threads=30 qtrim=l trimq=30   
```
<pre>
Output: Processing time 2159.406 seconds.   
Input:                  	991475034 reads 		   49573751700 bases <b>NB: 50-nt reads provided by workshop</b>   
QTrimmed:               	619503573 reads (62.48%) 	5263866092 bases (10.62%) - average of 8.5 bases per read q-trimmed   
KTrimmed:               	  8661402 reads (0.87%)      429209964 bases (0.87%)   
Total Removed:          	 89102972 reads (8.99%)     5693076056 bases (11.48%)   
Result:                 	902372062 reads (91.01%)   43880675644 bases (88.52%) - average length 48.6 nt   
</pre>
3. Use BWA v0.7.15 to align reads to v2.01 loblolly pine assembly (unmasked, unfiltered - no short contigs/scaffolds removed)

```console
bwa0715 mem -t 20 -T 30 ../ptaeda2/Lp.v201 r1.filt.fq.gz r2.filt.fq.gz | samtools view -buF2828 - |   
 samtools sort -l 5 -m1G -@30 -O 'bam' -T tmp -o v201.filt.bam -   
```

4. Use genomecov from Bedtools package to produce bedgraph directly from sorted BAM file of filtered reads
aligned to v2.01 assembly. Recover pita.v201.scaff.sizes from header of BAM file

```console
samtools view -H v201.filt.bam | awk '$1=="@SQ"{print substr($2,4)"\t"substr($3,4)}' > pita.v201.scaff.sizes   
genomeCoverageBed -bg -ibam v201.filt.bam -g pita.v201.scaff.sizes > v201.filt.pe.bedgraph   
```

5. Sort bedgraph file, then merge regions - calculate mean of coverage for each region during merge.   

```console
sort --parallel=30 -S 20G -k1,1 -k2n,2 v201.filt.pe.bedgraph | mergeBed -d 0 -c 4 -prec 2 -i stdin -o mean > v201.filt.pe.mergedCov.bed   
```

ATAC-seq
-------- 

1. Filter raw data to remove reads that are similar to mitochondrial or chloroplast sequences, also trim adapters - use BBTools bbduk.sh
with k-mer length 65 for filtering organellar sequences, pipe output to another process to remove adapters with k-mer 20.

```console
for N in {1..6}; do ~/software/bbmap/bbduk.sh k=65 t=10 qhdist=1 hdist=1 in=MiseqData/S${N}.r1.fq.gz in2=MiseqData/S${N}.r2.fq.gz out=stdout.fq  \   
 ref=/home/rosswhet/ptaeda2/pita.mito.scaffs.fa.gz,/home/rosswhet/ptaeda2/pita.cpDNA.fa.gz |   
 /home/rosswhet/software/bbmap/bbduk.sh k=20 t=10 qhdist=1 hdist=1 ktrim=r qtrim=rl trimq=10 \   
 ref=/home/rosswhet/software/bbmap/resources/nextera.fa.gz in=stdin.fq int=t out=MiseqData/S${N}.ntlv.fq.gz; done   

for N in {1..6}; do ~/software/bbmap/bbduk.sh k=65 t=10 qhdist=1 hdist=1 in=NextseqData/S${N}.r1.fq.gz in2=NextseqData/S${N}.r2.fq.gz out=stdout.fq \   
 ref=/home/rosswhet/ptaeda2/pita.mito.scaffs.fa.gz,/home/rosswhet/ptaeda2/pita.cpDNA.fa.gz |   
 /home/rosswhet/software/bbmap/bbduk.sh k=20 t=10 qhdist=1 hdist=1 ktrim=r qtrim=rl trimq=10 \   
 ref=/home/rosswhet/software/bbmap/resources/nextera.fa.gz in=stdin.fq int=t out=NextseqData/S${N}.ntlv.fq.gz; done   
```

2. Align filtered, trimmed reads to version 2.01 of the unmasked, unfiltered pine genome assembly.

```console
for N in {1..6}; do cat MiseqData/S${N}.ntlv.fq.gz NextseqData/S${N}.ntlv.fq.gz |   
 bwa0715 mem -t 20 -R "@RG\tID:S${N}\tSM:S${N}" -p /media/seagate5/ptaeda2/Lp.v201 - |   
 samtools view -bf3 - | samtools sort -n -m10G -@6 -T tmp -O 'bam' -o pita.v201/S${N}.nsrtv201.bam -; done   
```

3. Convert all BAM files to bedpe format; put jobs in background so all run in parallel

```console
bamToBed -bedpe -i S1.nsrtv201.bam 2> S1.unmat201.reads | gzip 1> S1.pe201.bed.gz &   
bamToBed -bedpe -i S2.nsrtv201.bam 2> S2.unmat201.reads | gzip 1> S2.pe201.bed.gz &   
bamToBed -bedpe -i S3.nsrtv201.bam 2> S3.unmat201.reads | gzip 1> S3.pe201.bed.gz &   
bamToBed -bedpe -i S4.nsrtv201.bam 2> S4.unmat201.reads | gzip 1> S4.pe201.bed.gz &   
bamToBed -bedpe -i S5.nsrtv201.bam 2> S5.unmat201.reads | gzip 1> S5.pe201.bed.gz &   
bamToBed -bedpe -i S6.nsrtv201.bam 2> S6.unmat201.reads | gzip 1> S6.pe201.bed.gz &   
```

4. Parse bed files to extract scaff ID, left end, right end, length, strand, and map q; filter for MAPQ > 20
Column names for bioawk BED format are   
*1:chrom	2:start 3:end 4:name 5:score 6:strand 7:thickstart 8:thickend 9:rgb 10:blockcount 11:blocksizes 12:blockstarts* and example data line is   
*scaffold30495	407083	407159	scaffold30495	407206	407282	M02019:79:000000000-APRDD:1:1101:2133:11832	60	+	-*   

Columns named "start" and "end" are start and end of left-most read; "score" and "strand" columns are start and end of right-most read; "thickend" is MAPQ.   
-t specifies input and output delimiters are tab. Bioawk can read gzipped files directly; write output files as uncompressed text.   

Recover only reads that map to same scaffold ($1==$4) with MAPQ> 20, otherwise skip over the read pair   

```console
bioawk -cbed -t '$thickend>20 && $1==$4{out=0; if($strand - $start < 140) out="S1.201.subnucl.bed"; else ($strand - $start < 250) ? out="S1.201.mononucl.bed" : out="S1.201.polynucl.bed"; print $chrom,$start,$strand,$strand-$start,$rgb,$thickend > out}' S1.pe201.bed.gz &   
bioawk -cbed -t '$thickend>20 && $1==$4{out=0; if($strand - $start < 140) out="S2.201.subnucl.bed"; else ($strand - $start < 250) ? out="S2.201.mononucl.bed" : out="S2.201.polynucl.bed"; print $chrom,$start,$strand,$strand-$start,$rgb,$thickend > out}' S2.pe201.bed.gz &   
bioawk -cbed -t '$thickend>20 && $1==$4{out=0; if($strand - $start < 140) out="S3.201.subnucl.bed"; else ($strand - $start < 250) ? out="S3.201.mononucl.bed" : out="S3.201.polynucl.bed"; print $chrom,$start,$strand,$strand-$start,$rgb,$thickend > out}' S3.pe201.bed.gz &   
bioawk -cbed -t '$thickend>20 && $1==$4{out=0; if($strand - $start < 140) out="S4.201.subnucl.bed"; else ($strand - $start < 250) ? out="S4.201.mononucl.bed" : out="S4.201.polynucl.bed"; print $chrom,$start,$strand,$strand-$start,$rgb,$thickend > out}' S4.pe201.bed.gz &   
bioawk -cbed -t '$thickend>20 && $1==$4{out=0; if($strand - $start < 140) out="S5.201.subnucl.bed"; else ($strand - $start < 250) ? out="S5.201.mononucl.bed" : out="S5.201.polynucl.bed"; print $chrom,$start,$strand,$strand-$start,$rgb,$thickend > out}' S5.pe201.bed.gz &   
bioawk -cbed -t '$thickend>20 && $1==$4{out=0; if($strand - $start < 140) out="S6.201.subnucl.bed"; else ($strand - $start < 250) ? out="S6.201.mononucl.bed" : out="S6.201.polynucl.bed"; print $chrom,$start,$strand,$strand-$start,$rgb,$thickend > out}' S6.pe201.bed.gz &   
```

5. Sort output bed files by scaffold ID and start position.

```console
for file in S1.201.*nucl.bed; do sort -S 10G -T ./ --parallel=7 -k1,1 -k2,2n $file > ${file%.*}.sort.bed; done &   
for file in S2.201.*nucl.bed; do sort -S 10G -T ./ --parallel=7 -k1,1 -k2,2n $file > ${file%.*}.sort.bed; done &   
for file in S3.201.*nucl.bed; do sort -S 10G -T ./ --parallel=7 -k1,1 -k2,2n $file > ${file%.*}.sort.bed; done &   
for file in S4.201.*nucl.bed; do sort -S 10G -T ./ --parallel=7 -k1,1 -k2,2n $file > ${file%.*}.sort.bed; done &   
for file in S5.201.*nucl.bed; do sort -S 10G -T ./ --parallel=7 -k1,1 -k2,2n $file > ${file%.*}.sort.bed; done &   
for file in S6.201.*nucl.bed; do sort -S 10G -T ./ --parallel=7 -k1,1 -k2,2n $file > ${file%.*}.sort.bed; done &   
```

6. To get coverage across union of regions from all six reps for each size-class of ATAC-seq insert, use
multiIntersectBed to combine bedfiles for all reps, then plan to use multicov with position-sorted, indexed BAM files to extract coverage

```console
multiIntersectBed -header -names S1 S2 S3 S4 S5 S6 -i S1.201.subnucl.sort.bed S2.201.subnucl.sort.bed S3.201.subnucl.sort.bed S4.201.subnucl.sort.bed S5.201.subnucl.sort.bed S6.201.subnucl.sort.bed > all6.201.subnucl.bed &   
multiIntersectBed -header -names S1 S2 S3 S4 S5 S6 -i S1.201.mononucl.sort.bed S2.201.mononucl.sort.bed S3.201.mononucl.sort.bed S4.201.mononucl.sort.bed S5.201.mononucl.sort.bed S6.201.mononucl.sort.bed > all6.201.mononucl.bed &   
multiIntersectBed -header -names S1 S2 S3 S4 S5 S6 -i S1.201.polynucl.sort.bed S2.201.polynucl.sort.bed S3.201.polynucl.sort.bed S4.201.polynucl.sort.bed S5.201.polynucl.sort.bed S6.201.polynucl.sort.bed > all6.201.polynucl.bed &   
```

7. Review of all6.201.polynucl.bed shows bookended regions that can be merged, e.g. C1000053	259	303 and 303 602, so run bedtools merge 
before multicov. MultiIntersectBed files have header line which must be removed using tail -n +2; keep only first 3 columns.
This will merge together adjacent intervals in subnucl.bed and mononucl.bed files into regions larger than the original target areas,
but that may be a useful way of summarizing data anyway.

```console
tail -n +2 all6.201.subnucl.bed | cut -f1-3  | bedtools merge -i stdin > all6.201.sub.merged.bed &   
tail -n +2 all6.201.mononucl.bed | cut -f1-3  | bedtools merge -i stdin > all6.201.monomerged.bed &   
tail -n +2 all6.201.polynucl.bed | cut -f1-3  | bedtools merge -i stdin > all6.201.polymerged.bed &   
```

8. Sort BAM files in parallel; index sorted files

```console
samtools sort -m 6G -T ./tmp1 -@ 9 -l 2 -o S1.posrtv201.bam S1.nsrtv201.bam &   
samtools sort -m 6G -T ./tmp2 -@ 9 -l 2 -o S2.posrtv201.bam S2.nsrtv201.bam &   
samtools sort -m 6G -T ./tmp3 -@ 9 -l 2 -o S3.posrtv201.bam S3.nsrtv201.bam &   
samtools sort -m 6G -T ./tmp4 -@ 9 -l 2 -o S4.posrtv201.bam S4.nsrtv201.bam &   
samtools sort -m 6G -T ./tmp5 -@ 9 -l 2 -o S5.posrtv201.bam S5.nsrtv201.bam &   
samtools sort -m 6G -T ./tmp6 -@ 9 -l 2 -o S6.posrtv201.bam S6.nsrtv201.bam &   

samtools index S1.posrtv201.bam &   
samtools index S2.posrtv201.bam &   
samtools index S3.posrtv201.bam &   
samtools index S4.posrtv201.bam &   
samtools index S5.posrtv201.bam &   
samtools index S6.posrtv201.bam &   
```   

9. Use MAPQ threshold of 20 to be consistent with MNase-seq summary file; run multicov with 6 BAM files per size class of inserts   

```console
bedtools multicov -q 20 -bams S1.posrtv201.bam S2.posrtv201.bam S3.posrtv201.bam S4.posrtv201.bam S5.posrtv201.bam S6.posrtv201.bam -bed all6.201.sub.merged.bed > all6.subnucl.cov.bed &   
bedtools multicov -q 20 -bams S1.posrtv201.bam S2.posrtv201.bam S3.posrtv201.bam S4.posrtv201.bam S5.posrtv201.bam S6.posrtv201.bam -bed all6.201.monomerged.bed > all6.mononucl.cov.bed &   
bedtools multicov -q 20 -bams S1.posrtv201.bam S2.posrtv201.bam S3.posrtv201.bam S4.posrtv201.bam S5.posrtv201.bam S6.posrtv201.bam -bed all6.201.polymerged.bed > all6.polynucl.cov.bed &   

wc -l all6.subnucl.cov.bed all6.mononucl.cov.bed all6.polynucl.cov.bed   
```

Output:   
* 2,883,089 all6.subnucl.cov.bed   
* 2,834,112 all6.mononucl.cov.bed   
* 2,933,464 all6.polynucl.cov.bed   
* 8,650,665 total    

10. Add columns with count of reps with coverage > 0 and total coverage to each file to have that information in downstream analyses   

```console
awk 'OFS="\t"{count=0;for(i=4;i<10;i++)if($i>0)count++}{print $0,count,($4+$5+$6+$7+$8+$9)}' all6.subnucl.cov.bed > all6.subn.covctsum.bed   
awk 'OFS="\t"{count=0;for(i=4;i<10;i++)if($i>0)count++}{print $0,count,($4+$5+$6+$7+$8+$9)}' all6.mononucl.cov.bed > all6.mono.covctsum.bed   
awk 'OFS="\t"{count=0;for(i=4;i<10;i++)if($i>0)count++}{print $0,count,($4+$5+$6+$7+$8+$9)}' all6.polynucl.cov.bed > all6.poly.covctsum.bed   
```

Merger of MNase-seq and ATAC-seq data files, summary of depth and overlaps
--------------------------------------------------------------------------

1. Intersect ATAC-seq size classes individually with MNase-Seq dataset using -wo option to keep all columns from both input files. Use ATAC-seq
file as "A" file, because -wo reports only A features with overlap. MNase-seq file has many more lines than ATAC-seq files do.

```console
intersectBed -wo -a all6.subn.covctsum.bed -b /media/seagate5/MNase/v201.filt.pe.mergedCov.bed > atac.subn.mnase.intsct.bed   

wc -l atac.subn.mnase.intsct.bed   
```

Output: 3,662,740 lines in atac.subn.mnase.intsct.bed    

2. Use datamash to summarize coverage by groups of reps detected (col 10)
in ATAC (col 11) and MNase-Seq (col 15).

```console
datamash -R 2 -s -g 10 mean 11 sstdev 11 mean 15 sstdev 15 count 10 < atac.subn.mnase.intsct.bed   
```

*Output is ATAC-seq mean and stdev of depth, then MNase-seq mean and stdev of depth, by number of ATAC-seq reps with coverage*    <pre>
 #rep   Amean   As.d.    Mmean   Ms.d.   count    
 1      2.53    1.06     3.52    3.31    836587.00 
 2      4.45    1.69     3.48    2.90    945575.00 
 3      6.79    2.55     3.47    3.35    795696.00 
 4      9.95    3.88     3.49    3.17    551052.00 
 5      15.01   6.63     3.56    3.83    334444.00 
 6      118.67 1535.39   5.01   16.98    199386.00     </pre>
*Not much difference in MNase-seq coverage until the last line; regions with coverage in all six ATAC-seq reps have deeper coverage for both data types*   

3. Extract subset of file with coverage in all 6 reps of ATAC-seq, using awk - add a column with length of ATAC-seq region

```console
awk '$10==6{print $0"\t"($3-$2)}' atac.subn.mnase.intsct.bed > atac_subnucl.MNase.intsct.bed   
```

4. Add a header line to that file using a text editor, then summarize length of ATAC-seq regions, ATAC-seq cov, MNase-seq cov, ATAC-MNase covariance, and sample stdev of ATAC and MNase coverages to allow calculation of correlation.

```console
datamash -R 2 -H scov 11:15 sstdev 11 sstdev 15 q1 16 median 16 q3 16 perc:95 16  max 16 q1 17 median 17 q3 17 perc:95 17 max 17 sum 16 sum 17< atac_subnucl.MNase.intsct.bed   
```

*Output*    <pre>
 scov(Mtot)   sstdev(Atot)   sstdev(Mtot)   
 8975.02        1535.39         16.98       </pre>   
*correlation of ATAC and MNase coverage depth is cov(x,y)/[sstdev(x)*sstdev(y)], or 8975/(1535.4*16.98) = 0.34; p<0.0001*   
<pre>
 q1(AMov)      median(AMov)  q3(AMov)     perc:95(AMov)  max(AMov) 
 49.00          89.00		  148.00	     273.00       2797.00    </pre>   
*distribution of lengths of ATAC-seq/MNase-seq overlapping regions*   
<pre>
 q1(Alen)    median(Alen)   q3(Alen)   perc:95(Alen)   max(Alen)  sum(AMov)   sum(Alen)  
 172.00        248.00        349.00      587.00         3924.00   21,722,062  56,920,713    </pre>   
*distribution of lengths of ATAC-seq regions, plus total lengths*   

5. Intersect mono-nucleosome-size insert ATAC-seq coverage data with MNase-seq coverage; summarize coverage depth by number of reps of ATAC-seq detected.

```console
intersectBed -wo -a all6.mono.covctsum.bed -b /media/seagate5/MNase/v201.filt.pe.mergedCov.bed > atac.mono.mnase.intsct.bed   
datamash -s -g 10 mean 11 sstdev 11 mean 15 sstdev 15 count 10 < atac.mono.mnase.intsct.bed   
```

*Output*    <pre>     
 #rep   Amean   As.d.    Mmean   Ms.d.   count     
 1      2.66    1.24    3.27    2.67    705318.00  
 2      5.12    2.08    3.25    2.73   1042877.00 
 3      8.17    3.18    3.22    2.59   1125852.00 
 4      12.38   4.92    3.20    2.55   1035091.00 
 5      19.42   8.67    3.18    2.65    879325.00  
 6      83.68   929.29  3.48    8.67    901858.00      </pre>    
*little if any difference in MNase-seq coverage even in regions covered in all 6 ATAC-seq reps*   

```console
intersectBed -wo -a all6.poly.covctsum.bed -b /media/seagate5/MNase/v201.filt.pe.mergedCov.bed > atac.poly.mnase.intsct.bed   
datamash -s -g 10 mean 11 sstdev 11 mean 15 sstdev 15 count 10 < atac.poly.mnase.intsct.bed   
```

*Output*    <pre>  
 #rep   Amean   As.d.    Mmean   Ms.d.   count     
 1       2.63    1.27    3.02    3.44    663936.00  
 2       5.59    2.34    3.05    2.34   1043754.00 
 3       9.25    3.60    3.02    4.26   1378370.00 
 4       14.3    5.63    3.00    2.34   1611412.00 
 5       23.02   10.12   2.98    2.14   1841802.00 
 6       90.77  620.00   3.03    4.92   3278903.00    </pre>
*Only difference in regions covered in all 6 ATAC-seq reps is higher S.D, not higher mean, for MNase-seq coverage*   


Extract regions with coverage above 99th percentile for MNase-seq OR ATAC-seq
-----------------------------------------------------------------------------

1. Find the depth of coverage in MNase-seq BEDfile filt.pe.mergedCov.bed that corresponds to 99th percentile

```console
datamash median 4 perc:75 4 perc:90 4 perc:95 4  perc:99 4 < /media/seagate5/MNase/filt.pe.mergedCov.bed
```
Output is <pre>
50	  75	90	  95	99    (%ile of MNase-seq coverage) 
2.2	  3.3	4.6	  5.4	7.7   (read depth) </pre>


2. What is total size of regions with MNase-seq coverage above 99th %ile? Export those regions to a new BEDfile

```console
awk '$4>7.7{total += ($3 - $2); count ++}END{print total, count}'  /media/seagate5/MNase/filt.pe.mergedCov.bed
```
Output is 305,875,343 bp in 977,112 regions

```console
awk '$4>7.7{print $1"\t"$2"\t"$3} /media/seagate5/MNase/filt.pe.mergedCov.bed > /media/seagate5/MNase/mnase.99pcile.bed
```

3. Find size of regions with 95th and 99th percentile coverage depths in all6.[subn|mono|poly].covctsum.bed files of ATAC-seq results

```console
datamash median 11 perc:95 11 perc:99 11 < all6.mono.covctsum.bed
```
Output is:<pre>
50	95	99
8	32	70 </pre>

```console
datamash median 11 perc:95 11 perc:99 11 < all6.poly.covctsum.bed
```
Output is: <pre>
50	95	99
12	59	135 </pre>

```console
datamash median 11 perc:95 11 perc:99 11 < all6.subn.covctsum.bed
```
Output is: <pre>
50	95	99
5	17	34 </pre>

```console
awk '$11>34{total+= ($3 - $2);count ++}END{print total, count}' all6.subn.covctsum.bed
awk '$11>70{total+= ($3 - $2); count ++}END{print total, count}'  all6.mono.covctsum.bed
awk '$11>125{total+= ($3 - $2); count ++}END{print total, count}'  all6.poly.covctsum.bed
```
Output is: <pre>
9,979,523 bp in 27835 regions at 99th %ile of subnucleosomal-size fragments
29,970,473 bp in 27879 regions at 99th %ile of mononucleosomal-size fragments
75,286,713 bp in 34110 regions at 99th %ile of polynucleosomal-size fragments </pre>


4. Make new BEDfiles with just the regions of coverage > 99th percentile in each insert size class

```console
awk '$11>34' all6.subn.covctsum.bed > atac.subn.99pcile.bed
awk '$11>70' all6.mono.covctsum.bed > atac.mono.99pcile.bed
awk '$11>125' all6.poly.covctsum.bed > atac.poly.99pcile.bed
```

Save the three ATAC-seq 99th-percentile BEDfiles and the corresponding MNase-seq 99th-percentile BEDfile in ConiferGenomes/Pita_10megaWGS








